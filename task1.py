from pulp import LpMaximize, LpProblem, LpStatus, lpSum, LpVariable

# Define the problem
model = LpProblem(name="maximize-number-of-products", sense=LpMaximize)

# Define the decision variables
x = LpVariable(name="Lemonade", lowBound=0)
y = LpVariable(name="Fruit_Juice", lowBound=0)

# Define the objective function
model += lpSum([x, y])

# Add constraints
model += (2 * x + y <= 100, "Water_constraint")
model += (x <= 50, "Sugar_constraint")
model += (x <= 30, "Lemon_Juice_constraint")
model += (2 * y <= 40, "Fruit_Puree_constraint")

# Solve the problem
model.solve()

# Print the optimal solution
print(f"Status: {LpStatus[model.status]}")
print(f"Lemonade = {x.varValue}")
print(f"Fruit Juice = {y.varValue}")
print(f"Maximum Number of Products = {model.objective.value()}")