import scipy.integrate as spi

# Define the function to be integrated, for example, f(x) = x^2
def f(x):
    return x**2

# Define the integration boundaries, for example, from 0 to 1
a = 0 # lower bound
b = 2 # upper bound

# Calculating the integral
result, error = spi.quad(f, a, b)

print("Integral: ", result)

import numpy as np

# Number of random points
N = 1000000

# Generate random points
x = np.random.uniform(a, b, N)
y = np.random.uniform(0, b**2, N)

# Determine the points under the curve
under_curve = y < f(x)

# Calculate the ratio and multiply by the total area
integral_monte_carlo = np.sum(under_curve) / N * (b**2 - 0) * (b - a)

print("Integral (Monte Carlo): ", integral_monte_carlo)

# Compare with the result from the quad function
print("Difference: ", abs(result - integral_monte_carlo))
print(f"Summary: The integral of the function was calculated analytically to be {result}. Using the Monte Carlo method with {N} random points, the integral was approximated to be {integral_monte_carlo}. The difference between these two methods is {abs(result - integral_monte_carlo)}, indicating the accuracy of the Monte Carlo method.")